﻿using Postlet.Common;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Postlet
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddPost : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private RelayCommand saveCommand;
        private RelayCommand cancelCommand;

        /// <summary>
        /// Gets the NavigationHelper used to aid in navigation and process lifetime management.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the DefaultViewModel. This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public RelayCommand SaveCommand
        {
            get
            {
                if (saveCommand == null)
                {
                    saveCommand = new RelayCommand(() => this.OnSavePost(), () => true);
                }
                return saveCommand;
            }
            set
            {
                saveCommand = value;
            }
        }

        public RelayCommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new RelayCommand(() => this.OnCancelPost(), () => true);
                }
                return cancelCommand;
            }
            set
            {
                cancelCommand = value;
            }
        }

        public AddPost()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
        }

        public void OnCancelPost()
        {
            this.Frame.GoBack();
        }

        public void OnSavePost()
        {
            this.Frame.GoBack();
        }
    }
}
