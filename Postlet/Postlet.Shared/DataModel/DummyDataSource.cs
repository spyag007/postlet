using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace Postlet.Data
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class DummyDataItem
    {
        public DummyDataItem(String uniqueId, String title, String kind, String date, String address, String price, String capacity, String phone, String contact, String note)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
			this.Kind = kind;
            this.Date = date;
            this.Address = address;
            this.Price = price;
            this.Capacity = capacity;
			this.Phone = phone;
			this.Contact = contact;
			this.Note = note;
			
			/*
			"UniqueId": "1",
			"Title": "Ban chung cu cao cap gia re",
			"Kind": "Ban",
			"Date": " 7/7/2015 ",
			"Address": "198A Ma Lo, Binh Tan, TPHCM",
			"Price": "100 trieu",
			"Capacity": "100m2",
			"Phone": "099999999",
			"Contact": "anh An",
			"Note": "Mien tiep trung gian"
			"*/
        }

        public string UniqueId { get; private set; }
        public string Title { get; private set; }
		public string Kind { get; private set; }
        public string Date { get; private set; }
        public string Address { get; private set; }
        public string Price { get; private set; }
        public string Capacity { get; private set; }
		public string Phone { get; private set; }
		public string Contact { get; private set; }
		public string Note { get; private set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// DummyDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public sealed class DummyDataSource
    {
        private static DummyDataSource _dummyDataSource = new DummyDataSource();

        private ObservableCollection<DummyDataItem> _items = new ObservableCollection<DummyDataItem>();
        public ObservableCollection<DummyDataItem> Items
        {
            get { return this._items; }
        }

        public static async Task<IEnumerable<DummyDataItem>> GetItemsAsync()
        {
            await _dummyDataSource.GetDummyDataAsync();

            return _dummyDataSource.Items;
        }

        public static async Task<DummyDataItem> GetItemAsync(string uniqueId)
        {
            await _dummyDataSource.GetDummyDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _dummyDataSource.Items.Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        private async Task GetDummyDataAsync()
        {
            if (this._items.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///DataModel/DummyData.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Items"].GetArray();
            
            foreach (JsonValue itemValue in jsonArray)
            {
  				JsonObject itemObject = itemValue.GetObject();
                
                this.Items.Add(new DummyDataItem(itemObject["UniqueId"].GetString(),
												   itemObject["Title"].GetString(),
												   itemObject["Kind"].GetString(),
												   itemObject["Date"].GetString(),
												   itemObject["Address"].GetString(),
												   itemObject["Price"].GetString(),
												   itemObject["Capacity"].GetString(),
												   itemObject["Phone"].GetString(),
												   itemObject["Contact"].GetString(),
												   itemObject["Note"].GetString()));
            }
        }
    }
}